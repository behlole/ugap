@extends('layouts.main')
@section('content')
@include('layouts.header')


<div class="innerbanner clearfix">
    <h3>SHOPPING CART</h3>
</div>



<div class="innerWbg clearfix">
    <div class="container clearfix">


        <div class="col-sm-9 clearfix" style="border:#F00 0px solid">



            <table id="cart" border="0" class="table table-hover table-condensed" style="border-bottom:#EEE 1px solid">
                <thead>
                    <tr style="background-color:#000;color:#fff;border:0px;">

                        <th width="38%">Product Name</th>
                        <th width="15%">Quantity</th>
                        <th width="22%">Price</th>
                        <th width="15%">G.Price</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                        $totalPrice=0;
                        $grandTotal=0;

                        $vat=$vat->vat;
                        $shippingPrice=0;
                    @endphp
                    @foreach ($data as $data)


                    <tr>

                        <input name="cartid" id="cartid" type="hidden" value="{{$data->id}}" />
                        <input name="productId" id="productId" type="hidden" value="{{$data->pid}}" />


                        <td data-th="Product Name">{{$data->productname}}
                            <p class="prcode">Part Number : {{$data->productcode}}</br>
                                Product Code : {{$data->productcode}}</p>
                        </td>


                        <td data-th="Quantity">
                            <div id="demoqty"></div>
                        <input type="number" name="quantity"  id="{{$data->order_id}}" class="form-control qtywidth special" value="{{$data->qty}}" >

                        </td>
                        @php
                            $totalPrice=$totalPrice+($data->productprice* $data->qty)
                        @endphp
                        <td data-th="Price">$ {{$data->productprice}} </td>
                         <td data-th="G. Price">
                                $ {{$data->productprice * $data->qty}}
                        </td>
                        <td data-th="Action">
                            <a href="/confirmorder?id=<?=base64_encode($data->id)?>&action=<?=base64_encode('Delete')?>"
                                class="btn btn-danger btn-sm" title="Delete">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach


                </tbody>
            </table>
            @php

                $vatPrice=$totalPrice/100 * $vat;

                $grandTotal=$totalPrice + $vatPrice;
            @endphp

            <table width="100%" border="0" style="text-align:right;">
                <tr>
                    <td><strong>Total Price : $ </strong> {{$totalPrice}}</td>
                </tr>
                <tr>
                    <td>
                        <strong>Vat : </strong> {{$vat}} %</td>
                </tr>
                <tr>
                <tr>
                    <td>
                        <strong>Shipping Price : $</strong>

                        {{$shipping}}


                    </td>
                </tr>

                <tr>
                    <td>

                        <strong>Grand Total : $</strong> {{$grandTotal + $shipping}}



                    </td>
                </tr>

                <tr>

                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
            </table>

            <a href="/placeOrder/{{Auth::user()->id}}" class="hvr-outline-outHund" style="text-align:center;">Checkout </a>

        </div>


        <div class="col-lg-3 clearfix" style="border:#F00 0px solid">





            @include('layouts.confirmAddress')
        </div>



    </div>
</div>
<script type="text/javascript">
  $(".special").on('change',function(e)
        {
             console.log("changed");
             console.log($(this).val());
            var id=$(this).attr("id");
            var qty=$(this).val();
            $.ajax({
            url: "/updateCartQuantity",
            method: "post",
            data: {
                'id':id,
                'qty':qty,
                '_token':"{{csrf_token()}}"
            },
            success: function (response) {

            console.log(response);
            window.location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
        });
        });
</script>
@include('layouts.footer')
@endsection
