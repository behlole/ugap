@extends('layouts.main')

@section('content')
@include('layouts.header')
<?php $pagename = substr($_SERVER['REQUEST_URI'], strrpos($_SERVER['REQUEST_URI'], '/') + 1); ?>

@foreach ($data as $data)
<div class="innerbanner clearfix">
    <div class="innerser">
        @include('layouts.searchfrm')
    </div>

    <h3>{{$data->heading}}</h3>
</div>


<div class="innerWbg clearfix">
    <div class="container clearfix">

        {!! $data->content !!}

    </div>
</div>
@endforeach


<div class="clients2 index-visible2  clearfix">
    @include('layouts.brands')
</div>

@include('layouts.footer')
@endsection