@extends('layouts.main')

@section('content')
@include('layouts.header')


<div class="innerbanner clearfix">
    <div class="innerser">
        @include('layouts.searchfrm')
    </div>

    <h3>Shopping Cart</h3>
</div>


<div class="innerWbg clearfix">
    <div class="container clearfix">
        @php
        $i=0;
        $totalPrice=0;

        @endphp
        <table id="cart" border="0" class="table table-hoverstyl  table-striped" style="border-bottom:#222 1px solid;">
            <thead>
                <tr style="background-color:#222;color:#fff;border:0px; ">
                    <th width="20%" style="">Product Name</th>
                    <th width="20%">Brand Name</th>
                    <th width="20%">Part Number</th>
                    <th width="10%">Quantity</th>
                    <th width="10%">Price</th>
                    <th width="10%">G.Price</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($data as $data)
                @php
                $i++;
                @endphp
                <input name="cartid" id="cartid" type="hidden" value={{$data->id}} />
                <input name="productId" id="productId" type="hidden" value={{$data->pid}} />


                <tr>


                    <td data-th="Product Name" style="">{{$data->productname}}
                        <p class="prcode">Product Code : {{$data->productcode}}</p>
                    </td>

                    <td data-th="Brand Name">{{$data->productname}}

                    </td>

                    <td data-th="Part Number">{{$data->partNumber}}

                    </td>

                    <td data-th="Quantity">

                        <input type="number" name="quantity"  id="{{$data->order_id}}" class="form-control qtywidth special" value="{{$data->qty}}" >
                    </td>

                    <td data-th="Price">$ {{$data->productprice}} </td>
                    @php
                        $totalPrice=$totalPrice+($data->productprice*$data->qty);
                    @endphp

                    <td data-th="G. Price">
                        {{$data->productprice * $data->qty}}
                    </td>
                    <td data-th="Action">

                        <a href="/deleteItem/{{$data->id}}" class="btn btn-danger btn-sm" title="Delete">
                            <i class="fa fa-trash-o"></i>
                        </a>

                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        <table width="100%" border="0" style="text-align:right;">

            <tr>
                <td><strong>Total Price : $ {{$totalPrice}}</strong></td>
            </tr>
             <tr>
                <td><strong>VAT :  {{$vat->vat}} %</strong></td>
            </tr>

            <tr>

                <td>

                    <strong>Grand Total : $ {{$totalPrice + (($vat->vat)/100 * $totalPrice)  }} </strong>
                     </td>
            </tr>
            <tr>

                <td>&nbsp;</td>
            </tr>
            <tr>
            <tr>

                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="right">

                    <!--<div class="col-lg-10 bottomP clearfix" style="border:#FF0000 0px solid;padding-right:0"><a href="index.php"><button type="button" class="btn btn-primary hvr-outline-out">Continue Shopping</button></a></div>-->

                    <div class="col-lg-10 bottomP clearfix" style="border:#FF0000 0px solid;padding-right:0">&nbsp;
                    </div>

                    <div class="col-lg-2 bottomP clearfix" style="border:#FF0000 0px solid; padding-right:0">

                        <button type="button" class="hvr-outline-outHund" id="open">Checkout + </button>
                        <button type="button" class="hvr-outline-outHund" id="hide" style="display:none;">Checkout -
                        </button>




                    </div>

                </td>
            </tr>

            <tr>
                <td>
                    <hr>
                </td>
            </tr>
        </table>

        @if ($i==0)
        Cart is Empty
        @endif
  <div class="col-lg-12 clearfix" style="display:none;" id="grlme">@include('layouts.checkout')</div>



        <!---------------------------------------------------------------------------------------------------------->




    </div>
</div>

<script>
    $(document).ready(function () {
        $("#open").click(function () {
            $("#grlme").show(1000);
            $("#open").hide();
            $("#hide").show();
        });
        $("#hide").click(function () {
            $("#grlme").hide(1000);
            $("#hide").hide();
            $("#open").show();

        });

        $(".special").on('change',function(e)
        {
             console.log("changed");
             console.log($(this).val());
            var id=$(this).attr("id");
            var qty=$(this).val();
            $.ajax({
            url: "/updateCartQuantity",
            method: "post",
            data: {
                'id':id,
                'qty':qty,
                '_token':"{{csrf_token()}}"
            },
            success: function (response) {

            console.log(response);
            window.location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
        });
        });


    });

</script>


</div>
</div>



<div class="clients2 index-visible2  clearfix">
    @include('layouts.brands')
</div>

@include('layouts.footer')
@endsection
