@extends('layouts.main')
@section('content')

@include('layouts.header')

<div class="innerbanner clearfix">
<h3>my account</h3>
</div>


<div class="innerWbg clearfix">
<div class="container clearfix">

<!-------------------------------------------------------------------------------- left Panel---------------------------------------------------------->
<div class="col-sm-3 clearfix">
    @include('layouts.leftMenu')
</div>

<!-------------------------------------------------------------------------------- Right Panel---------------------------------------------------------->
<div class="col-sm-9 clearfix" style="border:#F00 0px solid;">

<div class="col-sm-12 clearfix marginT20">


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">Full Name  </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
	  {{Auth::user()->name}}
	</div>
</div>


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">Last Name  </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
	  {{Auth::user()->lastName}}
	</div>
</div>


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">Email  </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
	  {{Auth::user()->email}}
	</div>
</div>


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">Contact Number  </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
	  {{Auth::user()->contact1}}
	</div>
</div>


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">Address  </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
	  {{Auth::user()->shippingAdress}}
	</div>
</div>


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">City </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
	  {{Auth::user()->city}}
	</div>
</div>


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">State  </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
	  {{Auth::user()->state}}
	</div>
</div>


<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

<div class="col-lg-5 col-sm-5  clearfix" style="border:#FF0000 0px solid">Country  </div>
	<div class="col-lg-7 col-sm-7 clearfix" style="border:#FF0000 0px solid">
        {{Auth::user()->country}}
	</div>
</div>



<p>&nbsp;</p>

<div class="row clearfix bottomP" style="border:#FF0000 0px solid;">

	<div class="col-lg-12 clearfix" style="border:#FF0000 0px solid">

        <a href="/editmyaccount" class="hvr-outline-outHund" style="color:#fff">
        	<img src="/images/png/glyphicons-152-new-window.png" class=""> Edit Profile
        </a>
    </div>
</div>

</div>

</div>
<!-------------------------------------------------------------------------------- Right Panel---------------------------------------------------------->
</div>
</div>

@include('layouts.footer')
@endsection
