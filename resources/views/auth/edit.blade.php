@extends('layouts.main')

@section('content')

@include('layouts.header')
<div class="innerbanner clearfix">
    <h3>Edit my account</h3>
</div>


<div class="innerWbg clearfix">
    <div class="container clearfix">
        <div class=" col-sm-3 clearfix">
            @include('layouts.leftMenu')
        </div>

        <div class="col-sm-9 clearfix">

            <div class="col-sm-12 clearfix marginT20">



                <form class="form-horizontal" name="editmyaccountfrm" id="editmyaccountfrm" action="editProfile" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{Auth::user()->id}}">
                    <div class="row clearfix bottomP msgpop">
                        <div class="col-lg-4  clearfix">&nbsp;</div>
                        <div class="col-lg-8  clearfix"></div>
                    </div>

                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix">Full Name *</div>
                        <div class="col-lg-8  clearfix">
                            <input id="fname" name="fname" class="for_texboxMyaccount" type="text"
                                value={{Auth::user()->name}} placeholder="Fill First Name" required="required" />
                        </div>
                    </div>

                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix">Last Name </div>
                        <div class="col-lg-8  clearfix">
                            <input id="lname" name="lname" class="for_texboxMyaccount" type="text"
                                value="{{Auth::user()->lastName}}" placeholder="Fill Last Name" />
                        </div>
                    </div>

                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix">Email *</div>
                        <div class="col-lg-8  clearfix">
                            <input id="email" name="email" placeholder="Fill Email Address" class="for_texboxMyaccount"
                                type="email" value={{Auth::user()->email}} readonly="readonly" required="required" />
                        </div>
                    </div>
                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix"> Contact Number *</div>
                        <div class="col-lg-8  clearfix">
                            <input id="contactnumber" name="contactnumber" placeholder="Fill Your Conatct Number"
                                class="for_texboxMyaccount" type="text" value={{Auth::user()->contact1}}
                                required="required" />
                        </div>
                    </div>
                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix"> Address *</div>
                        <div class="col-lg-8  clearfix">
                            <textarea id="address" name="address" placeholder="Fill Your Address"
                                class="for_textarea_autoedit"
                                required="required">{{Auth::user()->shippingAdress}}</textarea>
                        </div>
                    </div>
                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix"> City *</div>
                        <div class="col-lg-8  clearfix">
                            <input id="city" name="city" placeholder="Fill City" class="for_texboxMyaccount" type="text"
                                value={{Auth::user()->city}} required="required" />
                        </div>
                    </div>

                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix"> State </div>
                        <div class="col-lg-8  clearfix">
                            <input id="state" name="state" placeholder="Fill State" class="for_texboxMyaccount"
                                type="text" value={{Auth::user()->state}} />
                        </div>
                    </div>

                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix"> Country *</div>
                        <div class="col-lg-8  clearfix">
                            <div class="select-style3">
                                <select id="country" name="country" required="required">
                                    <option value="">Fill Your Country</option>
                                    <optgroup label="--------------------------">
                                        @foreach ($country as $country)
                                        @if (Auth::user()->country==$country->Country)
                                        <option value={{$country->Country}} selected>{{$country->Country}} </option>

                                        @else
                                        <option value={{$country->Country}}>{{$country->Country}} </option> @endif
                                        @endforeach </optgroup>
                                </select> </div>
                        </div>
                    </div>
                    <div class="row clearfix bottomP">
                        <div class="col-lg-4  clearfix">&nbsp;</div>
                        <div class="col-lg-8  clearfix">

                            <button id="submit" class="hvr-outline-outHund" name="submit" type="submit">
                                <img src="/images/png/glyphicons-152-new-window.png" style="width:20px; height:auto;">
                                Submit </button>

                        </div>
                    </div>


                </form>

            </div>

        </div>
        <!-------------------------------------------------------------------------------- Right Panel---------------------------------------------------------->
    </div>
</div>

@include('layouts.footer')
@endsection
