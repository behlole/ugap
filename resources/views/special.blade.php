@extends('layouts.main')

@section('content')
@include('layouts.header')
<?php  $pagename=substr($_SERVER['REQUEST_URI'], strrpos($_SERVER['REQUEST_URI'], '/') + 1);?>

<div class="innerbanner clearfix">
 <div class="innerser">
      @include('layouts.searchfrm')
 </div>

<h3>SPECIAL OFFERS</h3>
</div>


<div class="innerWbg clearfix">
<div class="container clearfix">


<table id="cart" border="0" class="table table-hoverstyl  table-striped"  style="border-bottom:#EEE 0px solid;text-align:center">
          <thead>
			  <tr style="background-color:#222;color:#fff;border:0px; ">
				<th width="12%" style="text-align:center">Brand Name</th>
				<th width="12%" style="text-align:center">Business Unit</th>
				<th width="15%" style="text-align:center">Part Number</th>
                <th width="16%" style="text-align:center">Part Name</th>
				<th width="11%" style="text-align:center">Weight(KG)</th>
				<th width="12%" style="text-align:center">Unit Price (USD)</th>
                <th width="10%" style="text-align:center">MOQ</th>
                @auth

                <th width="10%" style="text-align:center">Add to Cart</th>
                @endauth
			  </tr>
          </thead>
		  <tbody>

             @foreach ($data as $item)
                 <tr>
				<td data-th="Brand Name">{{$item->brandName}}</td>
                <td data-th="Business Unit">{{$item->businessUnit}}</td>
                <td data-th="Part Number">{{$item->partNumber}}</td>
                <td data-th="Part Name">{{$item->productname}}</td>
                <td data-th="Weight(KG)">{{$item->weight}}</td>
                <td data-th="Unit Price (USD)">$ {{$item->productprice}}</td>
                <td data-th="MOQ">{{$item->moq}}</td>
                    @auth
                <td data-th="Add to Cart"><a href="/cart/{{$item->id}}/{{ Auth::user()->id }}"> <img src="/images/cart.png"></a></td>
                @endauth
           </tr><tr style="border:none;background:none;" class="mobtr"><td colspan="8" style="background:none;">&nbsp;</td></tr>
             @endforeach


         </tbody>
         </table>

</div>
</div>



<div class="clients2 index-visible2  clearfix">
   @include('layouts.brands')
</div>

@include('layouts.footer')
@endsection
