@extends('layouts.main')
@section('content')
@include('layouts.header')


<div class="innerbanner clearfix">
    <h3>Change My Password</h3>
</div>


<div class="innerWbg clearfix">
    <div class="container clearfix">

        <!-------------------------------------------------------------------------------- left Panel---------------------------------------------------------->
        <div class=" col-sm-3 clearfix">
            @include('layouts.leftMenu')

        </div>



        <!-------------------------------------------------------------------------------- Right Panel---------------------------------------------------------->
        <div class="col-sm-9 clearfix">

            <div class="col-sm-12 clearfix marginT20">


                <form name="chpassfrm" id="chpassfrm" method="POST" action="/changePassword">>
                    @csrf
                    <input type="hidden" name="id" value={{Auth::user()->id}}>
                    <div class="row clearfix bottomP msgpop">
                        <div class="col-lg-4 clearfix">&nbsp;</div>
                        <div class="col-lg-8 clearfix"></div>
                    </div>

                    <div class="row clearfix bottomP">
                        <div class="col-lg-4 clearfix btrb_n">Password *</div>
                        <div class="col-lg-8 clearfix">
                            <input id="password" name="password" placeholder="Fill Password"
                                class="for_texboxMyaccount writeAlignR  @error('password') is-invalid @enderror" type="password" required="required"></div>

                                 @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>

                    <div class="row clearfix bottomP">
                        <div class="col-lg-4 clearfix btrb_n">Confirm Password *</div>
                        <div class="col-lg-8 clearfix"><input id="password_confirmation" name="password_confirmation"
                                placeholder="Fill Confirm Password" class="for_texboxMyaccount" type="password"
                                required="required"></div>
                    </div>


                    <div class="row clearfix bottomP">
                        <div class="col-lg-4 clearfix">&nbsp;</div>
                        <div class="col-lg-8 clearfix">

                            <button id="submit" class="hvr-outline-outHund" name="submit" type="submit">
                                <img src="/images/png/glyphicons-152-new-window.png" style="width:20px; height:auto;">
                                Submit </button>

                        </div>
                    </div>


                </form>

            </div>

        </div>
        <!-------------------------------------------------------------------------------- Right Panel---------------------------------------------------------->
    </div>
</div>
<script>
    @if(Session::has('message'))
var type="{{Session::get('alert-type','info')}}";
switch(type){
 case 'info':
 toastr.info("{{Session::get('message')}}");
 break;
 case 'success':
 toastr.success("{{Session::get('message')}}");
 break;
 case 'warning':
 toastr.warning("{{Session::get('message')}}");
 break;
}
@endif
</script>
@include('layouts.footer')
@endsection
