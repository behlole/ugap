<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalForgotPwdNewWindow"
    id="fpnw" style="display:none;">newwin</button>


<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalForgotPwd" id="fw"
    style="display:none;">forgotpwd</button>



<div class="modal-body modal-body-sub_agile clearfix">
    <div class="col-md-12 modal_body_left modal_body_left1 clearfix">

        <form method="post" name="forgotpwdfrm" id="forgotpwdfrm">

            <span class="msgpop">
                <center style="font-size:12px;"></center>
            </span>

            <div class="styled-input">
                <input type="email" name="emailid" id="emailid" required="required" placeholder="Email Id">
            </div>


            <div class="marginT20 clearfix">
                <button id="mymodalbtnforgotpassword" class="hvr-outline-outHund" name="mymodalbtnforgotpassword"
                    type="submit">
                    <img src="/images/png/glyphicons-152-new-window.png"
                        style="width:20px; height:auto;"> Submit </button>

            </div>


        </form>

        <div class="login-social-grids">
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
            </ul>
        </div>


    </div>
</div>
