
<!DOCTYPE html>
<html>
<head>
<title>UGAP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
<!-- Styles -->
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
<link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css" media="screen">
<!--[if lt IE 8]>
      <script src="/js/html5shiv.js"></script>
       <script src="/js/respond.min.js"></script>
<![endif]-->
<!--[if gte IE 7]>
	<link rel="stylesheet" type="text/css" href="/css/ie7-and-up.css" />
<![endif]-->


<script src="/js/jquery-latest.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/respond.src.js"></script>
<script src="/js/respond.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>

<link href="/css/jquery.smartmenus.bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery.smartmenus.js"></script>
<script type="text/javascript" src="/js/jquery.smartmenus.bootstrap.js"></script>


<!-- Insert to your webpage before the </head> -->
<script src="/sliderengine/amazingslider.js"></script>
<link rel="stylesheet" type="text/css" href="/sliderengine/amazingslider-1.css">
<script src="/sliderengine/initslider-1.js"></script>
<!-- End of head section HTML codes -->


 <!-- Insert to your webpage before the </head> -->
    <script src="/carouselengine/amazingcarousel.js"></script>
    <link rel="stylesheet" type="text/css" href="/carouselengine/initcarousel-1.css">
    <script src="/carouselengine/initcarousel-1.js"></script>
    <!-- End of head section HTML codes -->



<script>
$(window).scroll(function() {
  var sticky = $('.header'),
    scroll = $(window).scrollTop();

  if (scroll >= 40) {
    sticky.addClass('fixed'); }
  else {
   sticky.removeClass('fixed');

}
});


</script>
<script src="/js/venderemailcheck.js"></script>
</head>
<body>
@yield('content')
<!--Start header-->

</body>
<!--End header-->
