<div class="header clearfix">
  <div class="clearfix">
    <div class="logo col-sm-3"> <a href="/"><img src="{{asset('/uploads/logo/logo.png')}}" class="img-responsive"></a> </div>
    <div class="navbar navbar-default col-sm-9 clearfix" role="navigation" style="background:none; border:none;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      <div class="navbar-collapse collapse">
        @include('layouts.menu')
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>
</div>
