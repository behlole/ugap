<!-------------------------------------------Login Alert-------------------------------------------->
<div class="modal fade" id="myModalLoginAlert" tabindex="-1" role="dialog">
			<div class="modal-dialog clearfix">
				<!-- Modal content-->
				<div class="modal-content clearfix">
					<div class="modal-header clearfix">
                      <div class="col-md-10 logo_agileSub clearfix">
				         <h3>United German Auto Parts (UGAP)  </h3>
			            </div>

                      <div class="col-md-2 clearfix">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
					</div>
						<div class="modal-body clearfix">
                        <div class="col-md-12 clearfix"><img src="/images/logo.png" class="img-responsive" /></div>
						<div class="col-md-12 clearfix marginTop40"><center><span class="plzlogin">Please Login First.</span></center></div>
						<div style="clear:both"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>
<!-------------------------------------------Login Alert-------------------------------------------->
