
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal3" id="mymodalregbtn"
    style="display:none;">fb</button>

<div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <div class="signin-form profile">
                    <h3 class="agileinfo_sign">Sign Up</h3>
                    <div class="login-form">


                        <form  method="POST" action="{{ route('register') }}">
                            @csrf

                            <span class="msgpop">
                                <center style="font-size:12px;"></center>
                            </span>

                            <input id="name" type="text" class="@error('name') is-invalid @enderror" name="name"
                                value="{{ old('name') }}" required autocomplete="name" placeholder="Name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                            <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email"
                                value="{{ old('email') }}" placeholder="E-mail" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror


                            <input id="password" type="password" class="@error('password') is-invalid @enderror"
                                name="password" required autocomplete="new-password" placeholder="Password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">


                            <button id="regBtn" class="hvr-outline-outHund marginT20" name="regBtn" type="submit">
                                <img src="/images/png/glyphicons-152-new-window.png" style="width:20px; height:auto;">
                                Submit </button>
                        </form>
                    </div>
                    <div class="login-social-grids">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    @if (count($errors) > 0)
        $('#myModal3').modal('show');
    @endif
</script>
