<script language="JavaScript" type="text/javascript">
    function setPaymentInfo(isChecked) {
        with(window.document.checkout) {
            if (isChecked) {
                billingattention.value = shippingattention.value;
                billingphone.value = shippingphone.value;
                billingaddress.value = shippingaddress.value;
                billingcity.value = shippingcity.value;
                billingstate.value = shippingstate.value;
                billingcountry.value = shippingcountry.value;
                billingzipcode.value = shippingzipcode.value;

                billingattention.readOnly = true;
                billingphone.readOnly = true;
                billingaddress.readOnly = true;
                billingcity.readOnly = true;
                billingstate.readOnly = true;
                billingcountry.readOnly = true;
                billingzipcode.readOnly = true;
            } else {
                billingattention.readOnly = false;
                billingphone.readOnly = false;
                billingaddress.readOnly = false;
                billingcity.readOnly = false;
                billingstate.readOnly = false;
                billingcountry.readOnly = false;
                billingzipcode.readOnly = false;
            }
        }
    }

</script>



<form name="checkout" id="checkout" action="/shoppingCart" method="post">
    @csrf


    <div class="col-lg-6 clearfix">
        <div class="clearfix row bottomP">
            <div class="col-lg-12 shipping">Shipping Address</div>
        </div>
        <input type="hidden" name="user_id" value="{{$data->user_id}}">
        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Attention To*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid"><input id="shippingattention"
                    name="shippingattention" class="forsimple" type="text" name="attention1"
                    value="{{$data->attention1}}" required="required" />

            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Contact No.*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid"><input id="shippingphone" name="shippingphone"
                    class="forsimple" type="text" name="contact1" value="{{$data->contact1}}" required="required" />
            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Address*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid">
                <textarea id="shippingaddress" name="shippingaddress" class="forsimpletextare" name="shippingAddress"
                    required="required">{{$data->shippingAdress}}</textarea>
            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">City*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid"><input id="shippingcity" name="city"
                    class="forsimple" type="text" value="{{$data->city}}" required="required" />
            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">State</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid"><input id="shippingstate" name="state"
                    class="forsimple" type="text" value="{{$data->state}}" />
            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Country*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid">
                <div class="select-style3">
                    <select id="shippingcountry" name="shippingcountry" class="" name="country" required="required">
                        <option value="">Select The Country</option>
                        <optgroup label="--------------------------">
                            @foreach ($country as $country)
                            @if ($data->country==$country->Country)

                            <option value="{{$country->Country}}" selected>{{$country->Country}} </option>
                            @else
                            <option value="{{$country->Country}}">{{$country->Country}} </option>
                            @endif
                            @endforeach


                        </optgroup>
                    </select>
                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-6 clearfix">

        <div class="clearfix row bottomP">
            <div class="col-lg-12 shipping">Billing Address</div>
        </div>





        <div class="clearfix row bottomP">
            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Attention To*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid;">
                <input id="billingattention" name="billingattention" class="forsimple" type="text" name="attention2"
                    value="{{$data->attention2}}" required="required" />
            </div>
        </div>

        <div class="clearfix row bottomP">
            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Contact No.*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid"><input id="billingphone" name="contact2"
                    class="forsimple" type="text" value="{{$data->contact2}}" required="required">
            </div>
        </div>

        <div class="clearfix row bottomP">
            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Address*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid">
                <textarea id="billingaddress" name="billingaddress" class="forsimpletextare" name="address2"
                    required="required">{{$data->billingAdress}}</textarea>
            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">City*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid"><input id="billingcity" name="city2"
                    class="forsimple" type="text" value="{{$data->city2}}" required="required">
            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">State</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid"><input id="billingstate" name="state2"
                    class="forsimple" type="text" value="{{$data->state2}}" />
            </div>
        </div>

        <div class="clearfix row bottomP">

            <div class="col-lg-4 prqikHD" style="border: #0000CC 0px solid">Country*</div>
            <div class="col-lg-8" style="border: #0000CC 0px solid">
                <div class="select-style3">
                    <select id="billingcountry" name="country2" class="" required="required">
                        <option value="">Select The Country</option>
                        <optgroup label="--------------------------">

                            @foreach ($country1 as $country1)
                            @if ($data->country==$country1->Country)

                            <option value="{{$country1->Country}}" selected>{{$country1->Country}} </option>
                            @else
                            <option value="{{$country1->Country}}">{{$country1->Country}} </option>
                            @endif
                            @endforeach

                        </optgroup>
                    </select>
                </div>
            </div>
        </div>

    </div>






    <!--------------------------------------------------billing------------------------------------------------------------------------------>
    <div class="clearfix row bottomP">
        <div class="col-lg-12" style="border: #0000CC 0px solid">
            <hr class="hrcls" />
        </div>
    </div>

    <div class="clearfix row bottomP">

        <div class="col-lg-4" style="border: #0000CC 0px solid">&nbsp;</div>
        <div class="col-lg-8" style="border: #0000CC 0px solid"><input type="checkbox" name="chkSame" id="chkSame"
                value="1" onclick="setPaymentInfo(this.checked);" />&nbsp;&nbsp;
            My shipping and billing address are the same
        </div>
    </div>

    <div class="clearfix row bottomP">
        <div class="col-lg-12" style="border: #0000CC 0px solid">
            <hr class="hrcls" />
        </div>
    </div>


    <div class="clearfix row bottomP" align="center">
        <div class="col-lg-12" style="border: #0000CC 0px solid">
            <button id="submit" class="hvr-outline-outHund" name="submit" type="submit">
                <img src="/images/png/glyphicons-152-new-window.png" style="width:18px; height:auto">
                SUBMIT
            </button>

        </div>
    </div>
</form>
