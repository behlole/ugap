<!-----------------------forgot password----------------------------------------------------------------------->

<div class="modal fade" id="myModalForgotPwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header clearfix">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="signin-form profile">
                    <h3 class="agileinfo_sign">Forgot Password</h3>
                    @include('layouts.homeForgotPassword')

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</div>


<!-----------------------end forgot password------------------------------------------------------------------->



<!-----------------------forgot password----------------------------------------------------------------------->

<div class="modal fade" id="myModalForgotPwdNewWindow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

       <div class="modal-header clearfix">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="signin-form profile">
					<h3 class="agileinfo_sign">Reset Forgot Password</h3>
                        @include('layouts.homeForgotPasswordNewWindow')
    			</div>
    		</div>
    	</div>
    </div>
  </div>
</div>

<!-----------------------end forgot password------------------------------------------------------------------->
