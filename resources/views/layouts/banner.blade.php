<div class="clearfix">


<div id="amazingslider-wrapper-1" style="display:block;position:relative;width:100%;margin:0px auto 0px;">
        <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">
                @foreach ($banner as $banner)
                    <li><img src={{asset('storage/'.$banner->image_url)}} alt="United German Auto Parts (UGAP)"  title="United German Auto Parts (UGAP)" /></li>
                @endforeach
            </ul>
        </div>

  <div class="search">
        <div class="search-title">
            @foreach ($bannerText as $bannerText)

            <span>{{$bannerText->t1}}</span>
            {{$bannerText->t2}}
            @endforeach
        </div>

                @include('layouts.searchfrm')

    </div>
    </div>


</div>
