<div class="nav-side-menu">
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"
        style="display:none; cursor:pointer"></i>
    <div class="menu-list menu-content collapse in" id="menu-content">

        <div class="profile-sidebar">

            <form name="upicfrm" id="upicfrm" action="/updateImage" method="post" enctype="multipart/form-data">
                @csrf
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">



                    <div class="fileUpload">
                        <span>


                            <img src={{asset('storage/'.Auth::user()->avatar)}} class="img-responsive"
                                id="image_upload_preview2">
                            <input type="hidden" name="id" value={{Auth::user()->id}}>
                        </span>
                        <input type="file" class="upload" id="inputFile2" name="upic" />
                    </div>





                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{Auth::user()->name}}
                    </div>
                    <!--<div class="profile-usertitle-job">
						Developer
					</div>-->
                </div>




                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons" align="center">
                    <button type="submit" class="btn btn-success btn-sm" name="upicbtn" id="image_upload_btn"
                        style="display:none;">Upload</button>
                    <!--<button type="button" class="btn btn-danger btn-sm">Message</button>-->
                </div>

            </form>







            <!-- END SIDEBAR BUTTONS -->
            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu">
                <ul class="nav">
                    <li < class="active">
                        <a href="/myaccount">
                            <i class="glyphicon glyphicon-home top_nav_left"></i> Overview &nbsp;</a>
                    </li>
                    <li class="active">
                        <a href="/editmyaccount">
                            <i class="glyphicon glyphicon-user top_nav_left"></i>
                            Edit Profile&nbsp;</a>
                    </li>

                    <li class="active">
                        <a href="/changepassword">
                            <i class="glyphicon glyphicon-lock top_nav_left"></i>
                            Change Password &nbsp;</a>
                    </li>










                    <?php /*?><li <?php if($po=='sbaddress.php' || $po=='edit_sbaddress.php'){?> class="active">
                        <a href="sbaddress.php">
                            <i class="glyphicon glyphicon-map-marker top_nav_left"></i>
                            Shipping / Billing Address &nbsp;</a>
                    </li>


                    <li <?php if($po=='myorder.php' || $po=='orderdetails.php'){?> class="active">
                        <a href="myorder.php">
                            <i class="glyphicon glyphicon-tag top_nav_left"></i>
                            My Order &nbsp;</a>
                    </li>

                    <li <?php if($po=='mywishlist.php'){?> class="active">
                        <a href="mywishlist.php">
                            <i class="glyphicon glyphicon-heart top_nav_left"></i>
                            My Wishlist &nbsp;</a>
                    </li>
                    <?php */?>
                    <?php /*?> <li <?php if($po=='myreview.php'){?> class="active">
                        <a href="myreview.php">
                            <i class="glyphicon glyphicon-ok top_nav_left"></i>
                            My Review &nbsp;</a>
                    </li>
                    <?php */?>



                    <li>
                        <a href="logout">
                            <i class="glyphicon glyphicon-off top_nav_left"></i>
                            Logout &nbsp;</a>
                    </li>
                </ul>
            </div>

        </div>

    </div>
</div>



<script>
    //var $pup2 = jQuery.noConflict();
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview2').attr('src', e.target.result);
                $('#image_upload_btn').css('display', 'block');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputFile2").change(function () {
        readURL2(this);
    });

    $widthScr = $(window).width();
    if ($widthScr < 800) {
        $(".toggle-btn").css("display", "block");
        $("#menu-content").removeClass("in");
        $("#menu-content").addClass("out");

    }

</script>
