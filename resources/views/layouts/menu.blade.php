<ul class="nav navbar-nav">

    <li><a href="#">Main</a>


        <ul class="dropdown-menu">

            <li><a href="/" title="Home">Home</a></li>
            <li><a href={{ route('cms',['name'=>'about-us']) }} title="About us">About us</a></li>


            {{-- <?php /*?> <li><a href="#"><span onclick="window.location='/cms/<?=getPage(5)?>'"
                        style="margin-left:0px;">About us </span> <span class="caret" style="color:#fff"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/cms/<?=getPage(6)?>">Credit Reform</a></li>
                </ul>
            </li>

            <?php if($_SESSION['id']==''){?><li><a href="#" data-toggle="modal" data-target="#myModal3">Registration</a>
            </li><?php }?>

            <li><a href="/news">News</a></li>
            <li><a href="/cms/<?=getPage(7)?>">Delivery</a></li>
            <?php */?> --}}
        </ul>
    </li>


    <li><a href={{ route('cms',['name'=>'products']) }} title="Products">Products</a>


        <ul class="dropdown-menu">
            <li><a href={{route('cms',['name'=>'genuine-spare-parts']) }} title="GENUINE SPARE PARTS">GENUINE SPARE PARTS</a>
                <ul class="dropdown-menu">
                    @foreach ($brandName as $brand)


                    <li><a href={{route('brand',['name'=>$brand->brand_url]) }}>{{$brand->brand}}</a></li>

                    @endforeach
                </ul>
            </li>
            <?php /*?><li><a href="#">OEM Parts</a></li>
            <li><a href="#">Lubricants</a></li><?php */?>
        </ul>

    </li>
    <li><a href="#">Offices</a>
        <ul class="dropdown-menu">
            <li><a href={{route('cms',['name'=>'main-in-dubai'])}} title="main office in Dubai">main office in Dubai</a></li>
        </ul>

    </li>

    <li><a href="/contact" title="Contact">Contact </a>
        <!-- <ul class="dropdown-menu">
              <li><a href="#">Contact</a></li>
              <li><a href="#">Imprint</a></li>
              <li><a href="#">Legal Notes</a></li>
            </ul>-->
    </li>

    <?php /*?><li><a href="/hot-deals" class="deals"><img src="/images/hotdeals.png" class="img-responsive"></a></li>
    <?php */?>

    <li><a href="/specialOffers" title="special offers">special offers <span>|</span></a></li>
    @guest
    <li class="nav-item">

    <li><a href="#" data-toggle="modal" data-target="#myModal2" title="login">login</a></li>
    </li>
    @if (Route::has('register'))
    <li class="nav-item">
    <li><a href="#" data-toggle="modal" data-target="#myModal3" title="Register">Register <span>|</span></a> </li>

    </li>
    @endif
    @else




    <li><a href="/myaccount" title="My Account"> {{ Auth::user()->name }} </a></li>
    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            {{ __('Logout') }} <span>|</span></a> </li>


    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>


    @endguest








    <li><a href="#"><img src="/images/en.png"> <span>|</span></a>
        <ul class="dropdown-menu">
            <li><a href="#" title="English">English</a></li>
            <!--<li><a href="#">German</a></li>
              <li><a href="#">Russian</a></li>
              <li><a href="#">Ukrainian</a></li>
              <li><a href="#">Korean</a></li>
              <li><a href="#">Chinese</a></li>
              <li><a href="#">Spanish</a></li>-->
        </ul>
    </li>
    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>


    <li>@include('layouts.headerCart')</li>

</ul>
