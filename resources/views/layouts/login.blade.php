<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal2" id="mymodalbtn"
    style="display:none;">fb</button>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <div class="signin-form profile">
                    <h3 class="agileinfo_sign">Sign In</h3>
                    <div class="login-form">


                        <form method="POST" action="{{ route('login') }}" name="loginfrm" id="loginfrm">
                        @csrf

                            <span class="msgpop">
                                <center style="font-size:12px;"></center>
                            </span>

                            <input type="hidden" name="pag" id="pag">
                             <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-Mail" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror



                            <button id="loginbtn" class="hvr-outline-outHund marginT20" name="loginbtn" type="submit">
                                <img src="/images/png/glyphicons-152-new-window.png" /> Submit
                            </button>



                        </form>
                    </div>
                    <div class="login-social-grids">
    <ul>
        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i class="fa fa-rss"></i></a></li>
    </ul>
</div>



                    <div class="col-sm-6 clearfix"><a href="javascript:void(0)" data-toggle="modal"
                            data-target="#myModalForgotPwd" data-dismiss="modal" aria-label="Close"
                            style="cursor:pointer">Forgot Password</a></div>
                    <div class="col-sm-6 clearfix"><a href="#" data-toggle="modal" data-target="#myModal3"
                            data-dismiss="modal"> Don't have an account?'</a></div>

                </div>
            </div>
        </div>
    </div>
</div>

