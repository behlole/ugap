@extends('layouts.main')

@section('content')

@include('layouts.header')
@include('layouts.banner')
@include('layouts.mid')
@include('layouts.footer')
<script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}";
    switch (type) {
        case 'info':
            toastr.info("{{Session::get('message')}}");
            break;
        case 'success':
            toastr.success("{{Session::get('message')}}");
            break;
        case 'warning':
            toastr.warning("{{Session::get('message')}}");
            break;
    }
    @endif

</script>
@endsection
