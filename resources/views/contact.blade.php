@extends('layouts.main')

@section('content')
@include('layouts.header')
<div class="innerbanner clearfix">
    @foreach ($contact as $contact)

    <h3>{{$contact->heading}}</h3>
</div>




<div class="innerWbg clearfix">
    <div class="container clearfix">

        <div class="col-sm-6">{!! $contact->content !!}</div>
        @endforeach

        <div class="col-sm-6" style="border:0px solid #f00;">



            <form name="thfrm" id="thfrm" method="post" href="contact">
                @csrf
                <div class="col-sm-12">
                    <input name="name" id="name" type="text" class="simpleinputBox" placeholder="Name"
                        required="required">
                </div>

                <div class="col-sm-12">
                    <input name="telphone" id="telphone" type="text" class="simpleinputBox" placeholder="Mobile"
                        required="required">
                </div>

                <div class="col-sm-12">
                    <input name="email" id="email" type="email" class="simpleinputBox" placeholder="Email"
                        required="required">
                </div>

                <div class="col-sm-12">
                    <textarea name="mquery" id="mquery" cols="" rows="" class="simpleTextare"
                        placeholder="Message"></textarea>
                </div>


                <div class="col-sm-12 marginT10">
                    <button type="submit" name="submitbtn" id="submitbtn" class="hvr-outline-outHund"><img
                            src="/images/glyphicons-422-send.png"> Send Now !</button>
                </div>

            </form>

        </div>



    </div>
</div>


@include('layouts.footer')
<script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}";
    switch (type) {
        case 'info':
            toastr.info("{{Session::get('message')}}");
            break;
        case 'success':
            toastr.success("{{Session::get('message')}}");
            break;
        case 'warning':
            toastr.warning("{{Session::get('message')}}");
            break;
    }
    @endif

</script>
@endsection
