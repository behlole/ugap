<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $userName, $code, $total, $grandTotal;
    // Mail::to($this->email)->send(new OrderPlaced($this->userName, $this->totalPrice, $this->order_code, $this->grandTotal));

    public function __construct($name, $total1, $code, $grandTotal1)
    {
        $this->userName = $name;
        $this->total = $total1;
        $this->code = $code;
        $this->grandTotal = $grandTotal1;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('orderPlaced');
    }
}