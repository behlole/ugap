<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cm extends Model
{
    protected $table = "cms";
    protected $fillable = ['pagename', 'heading', 'cmsimage', 'shorttext', 'content', 'statis'];
}