<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id', 'hidden_cart_id', 'total', 'shippingFee', 'vat', 'grandTotal', 'shippingAddress', 'city', 'state', 'country', 'status'];
}