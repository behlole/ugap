<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Banner;
use App\Brand;
use App\Cart;
use App\Cm;
use App\Jobs\sendContactEmail;
use App\Product;
use Carbon\Carbon;
use App\User;
use App\Country;
use App\HiddenCart;
use App\Jobs\sendOrderMail;
use App\Order;
use App\Vat;
use Illuminate\Support\Facades\Hash;


class MainController extends Controller
{
    public function index()
    {
        $banner = Banner::where('status', '=', 'enable')->get();
        $bannerText = DB::table('banner_texts')->get();
        $brand = Brand::where('status', '=', 'Enabled')->get();
        $cms = Cm::where('status', '=', 'enable')->get();
        $brandNames = DB::table('brands')->get();
        $cart = DB::table('carts')->get();
        $count = count($cart);
        return view('welcome', ['banner' => $banner, 'bannerText' => $bannerText, 'brand' => $brand, 'cms' => $cms, 'brandName' => $brandNames, 'count' => $count]);
    }
    public function indexCMS($name)
    {
        $about = Cm::where('pageurl', '=', $name)->get();
        $brand = Brand::where('status', '=', 'Enabled')->get();
        $brandNames = DB::table('brands')->get();
        $cart = DB::table('carts')->get();

        $count = count($cart);

        return view('page', ['data' => $about, 'brand' => $brand, 'brandName' => $brandNames, 'count' => $count]);
    }
    public function indexBrand($name)
    {
        $about = Brand::where('brand_url', '=', $name)->get();
        $brand = Brand::where('status', '=', 'Enabled')->get();
        $brandNames = DB::table('brands')->get();
        $cart = DB::table('carts')->get();

        $count = count($cart);

        return view('brandPage', ['data' => $about, 'brand' => $brand, 'brandName' => $brandNames, 'count' => $count]);
    }
    public function contact()
    {
        $contact = Cm::where('pageurl', '=', 'contact-us')->get();
        $brandNames = DB::table('brands')->get();
        $cart = DB::table('carts')->get();

        $count = count($cart);

        return view('contact', ['contact' => $contact, 'brandName' => $brandNames, 'count' => $count]);
    }
    public function contactUs(Request $request)
    {

        // return $request->toArray();
        $data = $request->all();
        $email = $request->input('email');
        // <script>
        //     @if(Session::has('message'))
        // var type="{{Session::get('alert-type','info')}}";
        // switch(type){
        //  case 'info':
        //  toastr.info("{{Session::get('message')}}");
        //  break;
        //  case 'success':
        //  toastr.success("{{Session::get('message')}}");
        //  break;
        //  case 'warning':
        //  toastr.warning("{{Session::get('message')}}");
        //  break;
        // }
        // @endif
        // </script>
        dispatch(new sendContactEmail($data, $email))->delay(Carbon::now()->addSeconds(1));


        $notification = array(
            'message' => 'We will contact you soon',
            'alert-type' => 'success'
        );

        return back()->with($notification);
    }



    public function special()
    {

        $data = DB::table('products')->get();
        $brandNames = DB::table('brands')->get();
        $brand = Brand::where('status', '=', 'Enabled')->get();
        $cart = DB::table('carts')->get();
        $count = count($cart);
        return view('special', ['data' => $data, 'brandName' => $brandNames, 'brand' => $brand, 'count' => $count]);
    }
    public function search(Request $request)
    {
        $data = DB::table('products')->where('productcode', $request->input('code'))->first();
        $brandNames = DB::table('brands')->get();
        $brand = Brand::where('status', '=', 'Enabled')->get();
        $cart = DB::table('carts')->get();
        $count = count($cart);
        return view('searchProduct', ['data' => $data, 'brandName' => $brandNames, 'brand' => $brand, 'count' => $count]);
    }


    public function addToCart($item = null, $id = null)
    {

        #checking carted or not...
        if ($item != null && $id != null) {

            $check = DB::table('carts')->where('pid', $item)->where('user_id', $id)->first();
            if ($check == null) {
                $cart = new Cart();
                $itemData = DB::table('products')->where('id', $item)->first();
                $cart->order_id = rand();
                $cart->pid = $item;
                $cart->qty = 1;
                $cart->price = $itemData->productprice;
                $cart->status = "pending";
                $cart->user_id = $id;
                $cart->save();
            } else {
                $data = Cart::find($check->id);
                $data->qty = $data->qty + 1;
                $data->price = $data->price * $data->qty;
                $data->save();
            }
        }
        $vat = DB::table('vats')->where('id', 1)->first();

        $brand = Brand::where('status', '=', 'Enabled')->get();
        $brandNames = DB::table('brands')->get();
        // $data = Cart::where('user_id', '=', $id)->get();
        $data = DB::table('carts')->join('products', 'products.id', '=', 'carts.pid')->join('users', 'users.id', '=', 'carts.user_id')->get();
        $cart = DB::table('carts')->get();
        $country = Country::where('status', '=', 1)->get();
        $count = count($cart);

        // return json_decode($data);

        return view('cart')->with(['data' => $data, 'brand' => $brand, 'brandName' => $brandNames, 'count' => $count, 'country' => $country, 'country1' => $country, 'vat' => $vat]);
    }
    public function removeCart($id)
    {
        Cart::where('pid', $id)->delete();


        $brand = Brand::where('status', '=', 'Enabled')->get();
        $brandNames = DB::table('brands')->get();
        // $data = Cart::where('user_id', '=', $id)->get();
        $data = DB::table('carts')->join('products', 'products.id', '=', 'carts.pid')->join('users', 'users.id', '=', 'carts.user_id')->get();
        $cart = DB::table('carts')->get();
        $country = Country::where('status', '=', 1)->get();

        // return json_decode($data);
        $count = count($cart);

        return view('cart')->with(['data' => $data, 'brand' => $brand, 'brandName' => $brandNames, 'count' => $count, 'country' => $country, 'country1' => $country]);
    }
    public function showCart(Request $request)
    {

        $user = User::find($request->input('user_id'));

        $user->attention1 = $request->input('shippingattention');
        $user->contact1 = $request->input('shippingphone');
        $user->shippingAdress = $request->input('shippingaddress');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->country = $request->input('shippingcountry');





        $user->attention2 = $request->input('billingattention');
        $user->contact2 = $request->input('contact2');
        $user->billingAdress = $request->input('billingaddress');
        $user->city2 = $request->input('city2');



        $user->country2 = $request->input('country2');
        $user->state2 = $request->input('state2');

        $user->save();
        $user = DB::table('users')->where('id', $request->input('user_id'))->first();
        $shipping = $user->country;

        $brand = Brand::where('status', '=', 'Enabled')->get();
        $brandNames = DB::table('brands')->get();
        // $data = Cart::where('user_id', '=', $id)->get();
        $data = DB::table('carts')->join('products', 'products.id', '=', 'carts.pid')->join('users', 'users.id', '=', 'carts.user_id')->get();
        $cart = DB::table('carts')->get();

        $country = Country::where('status', '=', 1)->get();

        $shipping = DB::table('countries')->where('Country', $shipping)->first();


        $shipping = $shipping->shippingPrice;
        $count = count($cart);
        $vat = DB::table('vats')->where('id', 1)->first();
        // return json_decode($data);
        return view('finalCart')->with(['data' => $data, 'brand' => $brand, 'brandName' => $brandNames, 'count' => $count, 'vat' => $vat, 'country' => $country, 'country1' => $country, 'shipping' => $shipping]);
    }
    public function updateQty(Request $request)
    {
        DB::table('carts')->where('order_id', $request->input('id'))->update(['qty' => $request->input('qty')]);
        DB::table('carts')->where('qty', 0)->delete();
        return response()->json(['success' => 'data added succesfully\n']);
    }



    public function change(Request $request)
    {
        $user = $request->input('id');
        $data = User::find($user);
        if ($request->input('password') == $request->input('password_confirmation')) {



            $data->password = Hash::make($request->input('password'));
            $data->save();
            $notification = array(
                'message' => 'Password Changed successfully',
                'alert-type' => 'success'
            );
        } else {
            $notification = array(
                'message' => 'Password Confirmation not matched',
                'alert-type' => 'warning'
            );
        }
        return back()->with($notification);
        // return view('changePassword', ['brand' => $brand, 'brandName' => $brandNames, 'count' => $count]);

        // Hash::make($data['password'])
    }


    public function password()
    {
        $brand = Brand::where('status', '=', 'Enabled')->get();

        $brandNames = DB::table('brands')->get();
        $cart = DB::table('carts')->get();
        $count = count($cart);
        return view('changePassword', ['brand' => $brand, 'brandName' => $brandNames, 'count' => $count]);
    }
    public function account()
    {
        $brand = Brand::where('status', '=', 'Enabled')->get();

        $brandNames = DB::table('brands')->get();
        $cart = DB::table('carts')->get();
        $count = count($cart);
        return view('auth.myAccount', ['brand' => $brand, 'brandName' => $brandNames, 'count' => $count]);
    }
    public function updateImage(Request $request)
    {
        $employee = User::find($request->input('id'));
        if (isset($_FILES['upic'])) {
            $file = $request->file('upic');
            $name = $file->getClientOriginalName();
            $file->move('storage/users/', $name);

            if (file_exists(public_path($name =  $file->getClientOriginalName()))) {
                unlink(public_path($name));
            };
            //Update Image
            $employee->avatar = 'users/' . $name;
        }
        $employee->save();
        return redirect()->back();
    }
    public function returnEditPage()
    {
        $brandNames = DB::table('brands')->get();
        $brand = Brand::where('status', '=', 'Enabled')->get();

        $cart = DB::table('carts')->get();
        $count = count($cart);
        $country = DB::table('countries')->get();
        return view('auth.edit', ['brand' => $brand, 'brandName' => $brandNames, 'count' => $count, 'country' => $country]);
    }
    public function edit(Request $request)
    {
        $user = User::find($request->input('id'));
        $user->name = $request->input('fname');
        $user->lastName = $request->input('lname');
        $user->email = $request->input('email');
        $user->contact1 = $request->input('contactnumber');
        $user->shippingAdress = $request->input('address');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->country = $request->input('country');
        $user->save();
        return redirect()->back();
    }
    public function placeOrder($id)
    {
        $dataCart = Cart::where('user_id', '=', $id)->get();

        $dataUser = User::where('id', '=', $id)->first();
        $totalPrice = 0;

        $code = rand();
        foreach ($dataCart as $data) {
            $cart = new HiddenCart();
            $cart->order_code = $code;
            $cart->product_id = $data->pid;
            $cart->qty = $data->qty;
            $product = DB::table('products')->where('id', $data->pid)->first();
            $cart->price = $product->productprice;

            $cart->save();

            $totalPrice = $totalPrice + ($product->productprice * $data->qty);
        }
        $order = new Order();

        $order->user_id = $dataUser->id;
        $order->total = $totalPrice;
        $order->order_code = $code;
        // $hidden=HiddenCart::where('order_code','=',$code)->get();

        $order->shippingAddres = $dataUser->shippingAdress;
        $order->city = $dataUser->city;
        $order->state = $dataUser->state;
        $order->country = $dataUser->country;

        $cntry = DB::table('countries')->where('Country', $dataUser->country)->first();
        $order->shippingFee = $cntry->shippingPrice;

        $vat = Vat::where('id', '=', 1)->first();


        $order->status = "PENDING";


        $order->grandTotal = $cntry->shippingPrice + $totalPrice + ($totalPrice / 100 * $vat->vat);
        $order->save();
        $hidden = HiddenCart::where('order_code', '=', $code)->get();

        Cart::where('user_id', '=', $id)->delete();

        //      <h1>Hello {{Auth::user()->name}}</h1><br>
        // Your Order has been placed with order code : {{$order['order_code']}} <br>

        // Total Price : {{$order'total']}},
        // Grand Total : {{$order['grandTotal']}},

        Dispatch(new sendOrderMail($dataUser->name, $order->order_code, $order->total, $order->grandTotal,  $dataUser->email))->delay(Carbon::now()->addSeconds(1));
        $notification = array(
            'message' => 'Your order has been placed, We have sent you a confirmation mail',
            'alert-type' => 'success'
        );
        return redirect('/')->with($notification);
    }
}