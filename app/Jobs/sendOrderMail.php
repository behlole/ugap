<?php

namespace App\Jobs;

use App\Mail\OrderPlaced;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class sendOrderMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $totalPrice;
    public $order_code;
    public $email;
    public $grandTotal;
    public $userName;
    // Dispatch(new sendOrderMail($dataUser->name, $order->order_code, $order->total, $order->grand_total,  $dataUser->email))->delay(Carbon::now()->addSeconds(1));

    public function __construct($userName1, $orderCode, $totalPrice, $grandTotal1, $email)
    {
        $this->userName = $userName1;
        $this->totalPrice = $totalPrice;
        $this->order_code = $orderCode;
        $this->email = $email;
        $this->grandTotal = $grandTotal1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Mail::to($this->email)->send(new OrderPlaced($this->userName, $this->totalPrice, $this->order_code, $this->grandTotal));
    }
}