<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Banner extends Model
{
    protected $table = 'banners';
    protected $fillable = ['image_url', 'thumbnail', 'bannerHeading', 'burl', 'subHeading', 'specialComment', 'bannerContent', 'status'];
}