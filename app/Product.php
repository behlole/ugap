<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $table = "products";
    protected $fillable = ['category', 'product_url_title', 'productname', 'productcode', 'vat', 'productprice', 'brandName', 'businessUnit', 'partNumber', 'weight', 'moq', 'shortdep', 'description', 'productdata', 'porder', 'status'];
}