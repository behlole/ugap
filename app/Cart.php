<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cart extends Model
{
    protected $table = 'carts';
    protected $fillable = ['order_id', 'pid', 'qty', 'price', 'ship_price', 'coupon_code', 'status', 'status', 'user_id'];
}