<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = ['brand_url', 'brand_type', 'brand', 'brand_dep', 'status'];
}