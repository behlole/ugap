<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cms/{name}', 'MainController@indexCMS')->name('cms');
Route::get('/contact', 'MainController@contact');
Route::post('/contact', 'MainController@contactUs');
Route::get('/brand/{name}', 'MainController@indexBrand')->name('brand');
Route::get('/specialOffers', 'MainController@special');
Route::post('/searchProduct', 'MainController@search');


Route::get('/cart/{item?}/{id?}', 'MainController@addToCart')->middleware('auth');
Route::get('/deleteItem/{id}', 'MainController@removeCart')->middleware('auth');
Route::post('/shoppingCart', 'MainController@showCart')->middleware('auth');
Route::post('/updateCartQuantity', 'MainController@updateQty')->middleware('auth');
Route::get('/myaccount', 'MainController@account')->middleware('auth');
Route::post('/updateImage', 'MainController@updateImage')->middleware('auth');
Route::get('/editmyaccount', 'MainController@returnEditPage')->middleware('auth');
Route::post('/editProfile', 'MainController@edit')->middleware('auth');
Route::get('/placeOrder/{id}', 'MainController@placeOrder')->middleware('auth');
Route::get('/changepassword', 'MainController@password')->middleware('auth');
Route::post('/changePassword', 'MainController@change')->middleware('auth');
// Route::get('/cms/products', 'MainController@indexProducts');